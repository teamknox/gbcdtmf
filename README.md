# DTMF Dialer for GBC
This is the first GB software created by Yuichi Oda and also the first evidence that GB can be used for other purposes than a game machine.

<table>
<tr>
<td><img src="./pics/gb-dtmf.jpg"></td>
</tr>
</table>


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
